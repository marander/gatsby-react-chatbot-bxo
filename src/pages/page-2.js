import React, { Component } from "react";
import PropTypes from "prop-types";
import ChatBot from "react-simple-chatbot";
import { ThemeProvider } from "styled-components";

const theme = {
  background: "#f5f8fb",
  fontFamily: "Helvetica",
  headerBgColor: "#6d3dff",
  headerFontColor: "#fff",
  headerFontSize: "15px",
  botBubbleColor: "#6d3dff",
  botFontColor: "#fff",
  userBubbleColor: "#fff",
  userFontColor: "#4a4a4a"
};

class Review extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      gender: "",
      age: "",
      mail: ""
    };

    console.log(this.state);
  }

  UNSAFE_componentWillMount() {
    const { steps } = this.props;
    const { name, gender, age, mail } = steps;

    this.setState({ name, gender, age, mail });
  }

  render() {
    const { name, gender, age, mail } = this.state;
    return (
      <div style={{ width: "100%" }}>
        <h3>Summary</h3>
        <table>
          <tbody>
            <tr>
              <td>Name</td>
              <td>{name.value}</td>
            </tr>
            <tr>
              <td>Gender</td>
              <td>{gender.value}</td>
            </tr>
            <tr>
              <td>Alter</td>
              <td>{age.value}</td>
            </tr>
            <tr>
              <td>E-Mail</td>
              <td>{mail.value}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

Review.propTypes = {
  steps: PropTypes.object
};

Review.defaultProps = {
  steps: undefined
};

const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

class SimpleForm extends Component {
  UNSAFE_componentDidMount() {
    this.handleEnd = this.handleEnd.bind(this);
  }

  handleEnd({ steps, values }) {
    // console.log(steps);
    // console.log(values);
    alert(`Chat handleEnd callback! Number: ${values[(0, 1, 2, 3)]}`);
  }

  render() {
    return (
      <div>
        <ThemeProvider theme={theme}>
          <ChatBot
            handleEnd={this.handleEnd}
            steps={[
              {
                id: "1",
                message: "Hi, Welcome to Botxo. whats your name?!",
                trigger: "name"
              },
              {
                id: "name",
                user: true,
                trigger: "3"
              },
              {
                id: "3",
                message: "Hallo {previousValue}, state your gender",
                trigger: "gender"
              },
              {
                id: "gender",
                options: [
                  { value: "male", label: "outy", trigger: "5" },
                  { value: "female", label: "inny", trigger: "5" }
                ]
              },
              {
                id: "5",
                message: "How old are you?",
                trigger: "age"
              },
              {
                id: "age",
                user: true,
                trigger: "7",
                validator: value => {
                  if (isNaN(value)) {
                    return "Enter a number value.";
                  } else if (value < 0) {
                    return "Number cant be negative";
                  } else if (value > 120) {
                    return `${value}? Come on...`;
                  }

                  return true;
                }
              },
              {
                id: "7",
                message: "Let me grab your email adress real quick?",
                trigger: "mail"
              },
              {
                id: "mail",
                user: true,
                trigger: "9",
                validator: value => {
                  if (emailRegex.test(value) === false) {
                    return "Enter a valid email adress please";
                  }
                  return true;
                }
              },
              {
                id: "9",
                message:
                  "Super! ill send you this chat transcript and the other info aswell.",
                trigger: "review"
              },
              {
                id: "review",
                component: <Review />,
                asMessage: true,
                trigger: "update"
              },
              {
                id: "update",
                message: "Do you wish to correct something?",
                trigger: "update-question"
              },
              {
                id: "update-question",
                options: [
                  { value: "yes", label: "yes", trigger: "update-yes" },
                  { value: "no", label: "no", trigger: "end-message" }
                ]
              },
              {
                id: "update-yes",
                message: "Whitch field/entry do you want to edit?",
                trigger: "update-fields"
              },
              {
                id: "update-fields",
                options: [
                  { value: "name", label: "Name", trigger: "update-name" },
                  {
                    value: "gender",
                    label: "Gender",
                    trigger: "update-gender"
                  },
                  { value: "age", label: "Age", trigger: "update-age" },
                  { value: "email", label: "E-Mail", trigger: "update-email" }
                ]
              },
              {
                id: "update-name",
                update: "name",
                trigger: "9"
              },
              {
                id: "update-gender",
                update: "gender",
                trigger: "9"
              },
              {
                id: "update-age",
                update: "age",
                trigger: "9"
              },
              {
                id: "update-email",
                update: "email",
                trigger: "9"
              },
              {
                id: "end-message",
                message:
                  "Thanks for using BotXO, Your entry has been filed and relayed.",
                end: true
              }
            ]}
          />
        </ThemeProvider>
        <p>{JSON.stringify(this.states, null, 2)}</p>
      </div>
    );
  }
}

export default SimpleForm;
