module.exports = {
  siteMetadata: {
    title: `Gatsby-simple-chatbot-demo`,
    description: `Demo chatbots, This chatbot takes personaly and contant of a user and executs a trigger.`,
    author: `@webmaeistro`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-chatbot-demo`,
        short_name: `simple-chatbot`,
        start_url: `/`,
        background_color: `#f80e0e`,
        theme_color: `#f80e0e`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png` // This path is relative to the root of the site.
      }
    }
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ]
};
